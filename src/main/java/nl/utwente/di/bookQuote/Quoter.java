package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    double getBookPrice(String isbn) {
        HashMap<String, Double> books = new HashMap<>();
        books.put("1", 10.0);
        books.put("2", 45.0);
        books.put("3", 20.0);
        books.put("4", 35.0);
        books.put("5", 50.0);

        Double ret = books.get(isbn);
        if (ret == null) {
            ret = 0.0;
        }
        return ret;
    }
}
